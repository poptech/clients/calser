FROM registry.gitlab.com/poptech/dev-ops/wordpress:4.8.1

VOLUME ["/var/www/html"]

CMD ["apache2-foreground"]
