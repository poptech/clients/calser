#!/bin/sh

set -e

target=/var/www/html

# check if directory exists
if [ -d "$target" ]; then
    cd /tmp
    cp -vrf /wordpress/. /var/www/html
    cp -rf /tmp/code/. /var/www/html
else
    # directory doesn't exist, we will have to do something here
    echo need to creates the directory...
fi
chown -R www-data:www-data /var/www/html
exec "$@"
